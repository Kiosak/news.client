import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/shared/auth/auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  constructor(
    private authService: AuthService,
    private router: Router,
    private toastr: ToastrService
  ) {}

  public login = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
  });

  onSubmit() {
    if (this.login.valid) {
      const user = this.login.value;
      this.authService.login(user).subscribe(
        res => {
          if (res.success) {
            this.authService.setCurrentUser(res.user);
            this.router.navigateByUrl('/');
          }
        },
        err => this.toastr.error(err.error.message)
      );
    } else {
      this.toastr.error('Email and password required');
    }
  }

  ngOnInit() {}
}
