import { Component, OnInit } from '@angular/core';
import { ArticlesService } from 'src/app/services/articles.service';
import { Article } from 'src/app/models/article';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  constructor(private articleService: ArticlesService) {}

  public articles: Array<Article> = [];
  public isLoading = false;

  ngOnInit() {
    this.isLoading = true;
    this.articleService.getArticles().subscribe(
      data => {
        if (data.status === 'ok') {
          this.articles = data.articles;
        } else {
          console.log(data);
        }
      },
      error => console.log('err :', error),
      () => setTimeout(() => (this.isLoading = false), 2000)
    );
  }
}
