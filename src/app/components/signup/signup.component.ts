import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { AuthService } from 'src/app/shared/auth/auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {
  constructor(
    private authService: AuthService,
    private router: Router,
    private toastr: ToastrService
  ) {}

  public signup = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    email: new FormControl(''),
    password: new FormControl(''),
  });

  onSubmit() {
    if (this.signup.valid) {
      const user = {
        firstname: this.signup.value.firstName,
        lastname: this.signup.value.lastName,
        email: this.signup.value.email,
        password: this.signup.value.password,
      };
      this.authService.register(user).subscribe(
        res => {
          if (res.success) {
            this.authService.setCurrentUser(res.user);
            this.router.navigateByUrl('/');
          }
        },
        err => this.toastr.error(err.error.message)
      );
    } else {
      this.toastr.error('All fields are required');
    }
  }

  ngOnInit() {}
}
