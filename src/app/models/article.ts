export class Article {
  title: string;
  author: string;
  publishedAt: string;
  description: string;
  content: string;
  url: string;
  urlToImage: string;
  source: { name: string };
}

export class ArticleResponce {
  status: string;
  totalResults: number;
  articles: Article[];
}
