import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';

const usersEndpoint = environment.apiBaseUrl + '/users';
const httpOptions = {
  withCredentials: true,
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private storageUserData = localStorage.getItem('currentUser');
  public currentUser = new BehaviorSubject<any>(
    this.storageUserData ? JSON.parse(this.storageUserData) : null
  );

  constructor(private http: HttpClient) {}

  isLoggedIn() {
    return this.currentUser.value !== null;
  }

  register(user: object): Observable<any> {
    return this.http.post(usersEndpoint + '/register', { user }, httpOptions);
  }

  login(user: object): Observable<any> {
    return this.http.post(usersEndpoint + '/login', { user }, httpOptions);
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.currentUser.next(null);
  }

  setCurrentUser(user: object) {
    localStorage.setItem('currentUser', JSON.stringify(user));
    this.currentUser.next(user);
  }
}
