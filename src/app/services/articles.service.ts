import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ArticleResponce } from '../models/article';
import { Observable } from 'rxjs';

const HOST = 'https://newsapi.org/v2';
const API_KEY = 'apiKey=49dc9d611e8642538efbc9e896096daf';

@Injectable({
  providedIn: 'root',
})
export class ArticlesService {
  constructor(private http: HttpClient) {}

  getArticles(sources = 'techcrunch'): Observable<ArticleResponce> {
    return this.http.get<ArticleResponce>(`${HOST}/top-headlines?sources=${sources}&${API_KEY}`);
  }
}
